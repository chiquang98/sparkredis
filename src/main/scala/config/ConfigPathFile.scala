package config

import scala.io.Source

object ConfigPathFile {
  def readConfig(): List[String] = {
    val path = "config.txt"
    val lines = Source.fromFile(path).getLines().toList
    lines
  }
  def getOriginPath(): String = {
    val lines = readConfig()
    val originPath = lines(0).split("\\|")(1)
    originPath
  }
  def getHostRedis():String ={
    val lines = readConfig()
    val hostRedis = lines(1).split("\\|")(1)
    hostRedis
  }
  def getPortRedis():Int={
    val lines = readConfig()
    val portRedis = lines(2).split("\\|")(1)
    portRedis.toInt
  }
  def getNumberThread():Int={
    val lines = readConfig()
    val numberThread = lines(3).split("\\|")(1)
    numberThread.toInt
  }
  def getTypeIP(typeIP:Int):String={
    val lines = readConfig()
    if(typeIP==0){
      lines(4).split("\\|")(1)
    }
    else{
      lines(5).split("\\|")(1)
    }
  }
}
