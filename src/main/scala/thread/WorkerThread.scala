package thread
import redisdb.CacheRedis
class WorkerThread(day:String, indexIP:String, TTL:Int, key:String,typeIP:String) extends Runnable{
  override def run(): Unit = {
    CacheRedis.writeDataToCache(day,indexIP,TTL,key,typeIP)
  }
}
