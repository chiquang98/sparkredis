package thread

import java.util.concurrent.{ExecutorService, Executors, Future}

import config.ConfigPathFile

object FixedThreadPool {
  val NUM_OF_THREAD = ConfigPathFile.getNumberThread()
  val executor: ExecutorService = Executors.newFixedThreadPool(NUM_OF_THREAD)
  def executeWriteDataToCache(day:String,indexIP:String,TTL:Int,key:String,typeIP:String): Unit = {
    var worker = new WorkerThread(day,indexIP,TTL,key,typeIP)
    executor.execute(worker)

  }
}
