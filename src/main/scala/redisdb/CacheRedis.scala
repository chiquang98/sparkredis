package redisdb

import config.ConfigPathFile
import com.redis.RedisClient
import thread.FixedThreadPool
import serviceSpark.SparkCT
import java.nio.file.{Files, Paths}
object CacheRedis {
  val hostRedis = ConfigPathFile.getHostRedis()
  val port = ConfigPathFile.getPortRedis()
  val r = new RedisClient(hostRedis, port)

  def handleRequest(time: String, ip: String, typeIP: String): String = {
    //    2020122900 24_1
    var listRs: String = ""
    val TTL = 2 * 24 * 60 * 60 //Thời gian lưu dữ liệu vào cache là 2 ngày
    var day = time
    var requestAllDay = true //flag kiểm tra xem request cả ngày hay không
    if (time.length == 10) { // Nếu request time tới giờ
      day = time.substring(0, 8)
      requestAllDay = false // yêu cầu thông tin theo giờ thôi
    }
    val id = ip.lastIndexOf(".")
    val indexIP = ip.substring(id + 1)
    val key = typeIP + "_" + day + "_" + indexIP
    //println(key)
    // Thử kiểm tra trong cache redis xem có cache về ngày yêu cầu + IP kia không
    if (r.get(key).isEmpty) { //Nếu dữ liệu không có trong cache
      //println("none")
      //Lấy các thông tin đường dẫn cần thiết rồi trả về
      //Đồng thời cache cả ngày hôm đó vào redis
      //Create a Thread here
      /*
      val threadWriteDataToCache = new Thread {
        override def run: Unit = {
          writeDataToCache(day, indexIP, TTL,key)
        }
      }
      threadWriteDataToCache.start()
      */
      FixedThreadPool.executeWriteDataToCache(day, indexIP, TTL, key, typeIP)
    }
    else { //Nếu dữ liệu có trong cache
      //println("Có trong cache")
      //Trả về dữ liệu có trong cache
      if (requestAllDay) {
        //Lấy thông cả ngày
        //println("Lấy cả ngày")
        val hourAllDay = (0 to 23)
        hourAllDay.foreach(h => {
          var hour = h.toString
          if (h < 10) {
            hour = "0" + hour
          }
          val keyHour = typeIP + "_" + day + hour + "_" + indexIP
          val rsListHour = r.get(keyHour)
          if (rsListHour.isEmpty) {
            //println("Lon hon 0 " + keyHour)
            try{
              listRs = rsListHour.get
            }
            catch {
              case e:Exception=>{
                listRs = "No Data in DB"
              }
            }

          }
        })
      }
      else {
        //Lấy thông tin của giờ thôi
        //println("Lấy theo giờ")
        val keyHour = typeIP + "_" + time + "_" + indexIP
        //println(keyHour)
        try{
          val rsHour = r.get(keyHour).get
          listRs = rsHour
        }
        catch {
          case e:Exception=>{
            listRs = "No Data in DB"
          }
        }

      }
    }
    listRs
  }

  def writeDataToCache(day: String, indexIP: String, TTL: Int, key: String, typeIP: String): Unit = {
    val pathOrigin = ConfigPathFile.getOriginPath()
    val hourDay = (0 to 23)
    val SparkSS = SparkCT.sparkSession
    var flag = false
    hourDay.foreach(f => {
      var hour = f.toString
      if (f < 10) {
        hour = "0" + f.toString
      }
      var pathHour = pathOrigin + "/" + typeIP + "/" + day + hour + "/index=" + indexIP
      val configPathHour = Paths.get(pathHour)
      if (Files.exists(configPathHour)) { //Nếu tồn tại thư mục thì mới vào
        //println(pathHour)
        var keyTimeHour = typeIP + "_" + day + hour + "_" + indexIP
        if(typeIP.compareToIgnoreCase(ConfigPathFile.getTypeIP(0))==0){
          val df = SparkSS.read.parquet(pathHour)
          val rsJSON = df.toJSON.collect().toList
          val tmp = rsJSON.toString()
          val ind = tmp.lastIndexOf(")")
          val tempRS = "["+tmp.substring(5,ind)+"]"
          r.set(keyTimeHour, tempRS)
          r.expire(keyTimeHour, TTL)
        }
        else{
          val dfDestIP = SparkSS.read.parquet(pathHour).select("PhoneNumber")
          val rsJSON = dfDestIP.toJSON.collect().toList
          val tmp = rsJSON.toString()
          val ind = tmp.lastIndexOf(")")
          val tempRS = "["+tmp.substring(5,ind)+"]"
          r.set(keyTimeHour, tempRS)
          r.expire(keyTimeHour, TTL)
        }
      flag = true
      }
    })
    if(flag==true){
//      //println("IN TRUE")
      r.set(key, "YES") //Lưu YES cho ngày hôm đó tức là ngày đó đã được cache dữ liệu
      r.expire(key, TTL) // save two day in cache
    }
  }

}
