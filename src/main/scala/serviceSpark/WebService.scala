package serviceSpark
import config.ConfigPathFile
import org.scalatra.ScalatraServlet
import redisdb.CacheRedis
class WebService extends ScalatraServlet {
  //  http://localhost:8080/ipdest/?ip_dest=159.153.242.0&time=2020122908
  get("/ipdest/") { //Linh
    try{
      val ip = params.get("ip_dest").get
      val time = params.get("time").get
      val typeIP = ConfigPathFile.getTypeIP(1)
      val responseList = CacheRedis.handleRequest(time, ip, typeIP)
      if (responseList.compareToIgnoreCase("") == 0) {
        "NONE"
      }
      else {
        responseList
      }
    }
    catch {
      case e:Exception=>{

      }
    }

  }
  //  http://localhost:8080/ippublic/?ip_public=159.153.242.0&time=2020122908
  get("/ippublic/") { // Quang
    try{
      val ip = params.get("ip_public").get
      val time = params.get("time").get
      val typeIP = ConfigPathFile.getTypeIP(0)
      val responseList = CacheRedis.handleRequest(time, ip, typeIP)
      if (responseList.compareToIgnoreCase("") == 0) {
        "NONE"
      }
      else {
        responseList
      }
    }
    catch {
      case e:Exception=>{

      }
    }
  }
}