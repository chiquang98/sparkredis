Nếu trong database không có thư mục kết quả sẽ trả về: No data in DB
Nếu dữ liệu chưa có trong cache kết quả sẽ trả về: None
Nếu kết quả có trong cache: Kết quả trả về json
Các cấu trúc file đường dẫn: Sửa trong file config.txt bên trong project
uri test ip public: http://localhost:8080/ippublic/?ip_public=159.153.242.0&time=2020122908
uri Test ip dest: http://localhost:8080/ipdest/?ip_dest=159.153.242.0&time=2020122908

Giải thích file config:
originpath: đường dẫn chứa thư mục ippublic và ipdest
publicIP_folder_name: Tên thư mục chứa data của các thư mục index theo ippublic
destIP_folder_name: Tên thư mục chứa data của các thư mục index theo ipdest
numberThread: Số luồng tối đa đồng thời xử lý, nếu nhiều hơn sẽ xếp vào hàng đợi xử lý

Anh check giúp em xem luồng giải quyết dưới đây đã theo đúng ý anh chưa ạ.
luồng giải quyết:
- Trong thư mục origin path sẽ chứa 2 thư mục cho ippublic và ipdest
- với IP public, khi truyền vào API sẽ chỉ lấy chỉ số cuối, ví dụ 159.153.242.0, sẽ lấy chỉ số cuối là 0 để tìm thư mục
có index=0, nằm trong ngày với key time, ví dụ time = 2020122908, thì sẽ lấy hết dữ liệu index = 0 nằm trong thư mục
2020122908 rồi trả về dữ liệu dạng Json
- Ip dest: luồng giải quyết tương tự Ip Public nhưng chỉ trả về cột PhoneNumber.
Nếu cần sửa thêm gì anh ping em để em fix nhé anh.
